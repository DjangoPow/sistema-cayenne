$(function(){
	function Adicionar(){
		$("#tblCadastro tbody").append(
			"<tr class='tabela_produtos' style='border-spacing: 10px 10px;'>"+
			"<td class='tabela_produtos' style='width: 200px;'><input placeholder=' Nome do Produto' class='topo' type='text'/></td>"+
			"<td class='tabela_produtos linha-inativa' style='width: 200px;'><input placeholder=' Quantidade' class='topo' type='text'/></td>"+
			"<td class='tabela_produtos linha-inativa' style='width: 200px;'><input placeholder=' Valor unitário' class='topo' type='text'/></td>"+
			"<td class='tabela_produtos valor' >Valor total: R$ 45,87.00</td>"+
			"<td class='botao_tabela tabela_produtos'><img src='img/salvar.png' class='btnSalvar' style='margin-right:5px;'><img src='img/x.png' class='btnExcluir' style='margin-right:5px;/></td>"+
			"</tr>");

		$(".btnSalvar").bind("click", Salvar);
		$(".btnExcluir").bind("click", Excluir);
	};

	function Salvar(){
		var par = $(this).parent().parent(); //tr
		var tdNome = par.children("td:nth-child(1)");
		var tdQuantidade = par.children("td:nth-child(2)");
		var tdValor = par.children("td:nth-child(3)");
		var tdTotal = par.children("td:nth-child(4)");
		var tdBotoes = par.children("td:nth-child(5)");

		tdNome.html(tdNome.children("input[type=text]").val());
		tdQuantidade.html(tdQuantidade.children("input[type=text]").val());
		tdValor.html(tdValor.children("input[type=text]").val());
		tdTotal.html("Valor total: R$ 45,87.00");
		tdBotoes.html("<img src='img/x.png'class='btnExcluir'/><img src='img/editar.png' class='btnEditar' style='margin-right:5px;'/>");

		$(".btnEditar").bind("click", Editar);
		$(".btnExcluir").bind("click", Excluir);
	};

	function Editar(){
		var par = $(this).parent().parent(); //tr
		var tdNome = par.children("td:nth-child(1)");
		var tdQuantidade = par.children("td:nth-child(2)");
		var tdValor = par.children("td:nth-child(3)");
		var tdTotal = par.children("td:nth-child(4)");
		var tdBotoes = par.children("td:nth-child(5)");


		tdNome.html("<input type='text' id='txtNome' value='"+tdNome.html()+"'/>");
		tdQuantidade.html("<input type='text'id='txtTelefone' value='"+tdQuantidade.html()+"'/>");
		tdValor.html("<input type='text' id='txtEmail' value='"+tdValor.html()+"'/>");
		tdBotoes.html("<img src='img/salvar.png' class='btnSalvar'/>");

		$(".btnSalvar").bind("click", Salvar);
		$(".btnEditar").bind("click", Editar);
		$(".btnExcluir").bind("click", Excluir);
	};

	function Excluir(){
	    var par = $(this).parent().parent(); //tr
	    par.remove();
	};

	$(".btnEditar").bind("click", Editar);
	$(".btnExcluir").bind("click", Excluir);
	$("#btnAdicionar").bind("click", Adicionar);
});










      function InEn() {
        if (document.getElementById('entrega').checked) {
          document.getElementById('frete').style.display = "";
          document.getElementById('frete-2').style.display = "";
        }
        else {
          document.getElementById('frete').style.display = "none";
          document.getElementById('frete-2').style.display = "none";
        }
      }

      function RepLanca() {
        if (document.getElementById('Lanca').checked) {
          document.getElementById('mensalmente').style.display = "";
        }
        else {
          document.getElementById('mensalmente').style.display = "none";
        }
      }

      function VeMen() {
        if (document.getElementById('nvezes').checked) {
            document.getElementById('mensalmente-select').style.display = "";
            document.getElementById('mensalmente').style.display = "";
            document.getElementById('mensalmente-2').style.display = "none";
            document.getElementById('vezes').style.display = "none";
            document.getElementById('vezes-input').style.display = "";
        }
        else {
          document.getElementById('vezes-input').style.display = "none";
          document.getElementById('mensalmente-select').style.display = "none";
        }
      }
